
# Swipemax.com

Website revamp for swipemax.com

### Objective 👀
The objective of this product is to build a website that displays the design capabilities and tech prowess of [swipemax.com](http://swipemax.com) that would make the company a more reputable and noteworthy venture.

### MVP 🎯
Website that represents the Swipemax's identity.

### Tech Stack
- React
- GSAP
- HTML5
- WebGL ?

## Quick Start

The project runs on the create-react-app platform. To run this project locally just clone/download repository. `cd` into the directory in the terminal and run one of the following commands based on what you want:

`npm start` -- serves the project locally on port 8000

`npm test` -- No testing platform implemented yet

`npm run build` -- Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.Your app is ready to be deployed!

[more on deployment for c](https://facebook.github.io/create-react-app/docs/deployment) 


## Documentation

[Design Spec](./src/docs/design-spec.md)

[Coding Guidelines](./src/docs/coding-guidelines.md)

[Project break down and tasks](./src/docs/project-breakdown.md)

## Boards

[Swipe Website Board](./Swipe-Website-Board-13e47f90-4468-4161-8286-991c9cd07d49.csv)