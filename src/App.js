import React, { Component } from "react";
import About from './pages/About'
import Career from './pages/Career'
import Contact from './pages/Contact'
import Projects from './pages/Projects'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import * as Loaded from "./utils/page-loader";
import ScrollReset from "./utils/page-scroll-reset";
import "./App.css";

class App extends Component {
    render() {
        return (
            <Router>
                <ScrollReset>
                    <Switch>
                        <Route path="/" exact component={Loaded.Home} />
                        <Route path="/about" exact component={About} />
                        <Route path="/career" exact component={Career} />
                        <Route path="/contact" exact component={Contact} />
                        <Route path="/projects" exact component={Projects} />
                    </Switch>
                </ScrollReset>
            </Router>
        );
    }
}

export default App;
