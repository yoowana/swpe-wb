import React from 'react'
import * as Clients from './style.js'
import * as Type from "../../style/typeface"
import logos from "./logos";

export default () => {
    return (
        <Clients.Container>
            <Clients.Title> 
                <Type.H2>We’ve had the pleasure to work with:</Type.H2> 
            </Clients.Title>
            <Clients.ImgContainer>
            {
                logos.map((image, i) => {
                    return <Clients.Mage src={image.src} alt={image.alt} key={i} />
                })
            }
            </Clients.ImgContainer>
        </Clients.Container>
    )
}
