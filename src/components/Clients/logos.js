export default [
    {
        src: require("../../assets/svgs/Logos/firstbank.svg"),
        alt: "first bank"
    },
    {
        src: require("../../assets/svgs/Logos/stan.png"),
        alt: "Stanbic"
    },
    {
        src: require("../../assets/svgs/Logos/ren.svg"),
        alt: "renmoney"
    },
    {
        src: require("../../assets/svgs/Logos/linda.svg"),
        alt: "linda ikeji"
    },
]