import styled from 'styled-components'
import colors from '../../style/colors';

export const Container = styled.div`
    background-color: ${ colors.gray };
    padding: 40px 0;
`
export const Title = styled.div`
    display: block;
    text-align: center;
`
export const ImgContainer = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    padding: 40px 20px;
`
export const ImgOuter = styled.div`
    display: flex; 
    flex-direction: center;
    padding: 40px 0;
`
export const Mage = styled.img`
    width: 200px;
    object-fit: contain;
    height: 100px;
`