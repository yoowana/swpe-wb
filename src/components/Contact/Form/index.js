import React, { Component } from 'react'
import * as Form from './style'


export default class ContactForm extends Component {
    state = {
        user: {
            name: '',
            email: '',
            phone: '',
            company: '',
            budget: '',
            type: '',
            comment: ''
        }
    }

    handleChange = e => {
        this.setState({
            user: {
                ...this.state.user,
                [e.target.name]: e.target.value
            }
        });
    }

    handleSubmit = e => {
        e.preventDefault()
        console.log(this.state.user)
    }
    
    render() {
        return (
            <Form.Container>
                <form onSubmit={ this.handleSubmit}>
                    <Form.Pair>
                        <Form.Input name="name" placeholder="name"  onChange={this.handleChange} />
                        <Form.Input name="email" placeholder="email"  onChange={this.handleChange} />
                    </Form.Pair>
                    <Form.Pair>
                        <Form.Input name="phone" placeholder="phone"  onChange={this.handleChange} />
                        <Form.Input name="phone" placeholder="company"  onChange={this.handleChange} />
                    </Form.Pair>
                    <Form.Pair>
                        <Form.Select name="budget" placeholder="Budget" onChange={this.handleChange} >
                            <option> Budget </option>
                            <option> 500,000 </option>
                            <option> 1,000,000 </option>
                            <option> 10,000,000 </option>
                            <option> 100,000,000 </option>
                        </Form.Select>

                        <Form.Select name="type" placeholder="Project type" onChange={this.handleChange} >
                            <option> Project Type </option>
                            <option> Mobile Platform </option>
                            <option> IOT </option>
                            <option> Web Application </option>
                            <option> Branding &amp; Design </option>
                        </Form.Select>
                    </Form.Pair>
                    <Form.Pair>
                        <Form.TextArea 
                            name="comment"
                            placeholder="Breif description of your project and requirements."
                            onChange={ this.handleChange } 
                        />
                    </Form.Pair>
                    <Form.Pair>
                        <Form.Submit>Send</Form.Submit>
                    </Form.Pair>
                </form>
            </Form.Container>
        );
    }
}
