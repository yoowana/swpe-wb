import styled from "styled-components";
import colors from "../../../style/colors";

const margin = "15px";

export const Input = styled.input`
    outline: none;
    border: 2px solid #cdcdcd;
    text-decoration: none;
    flex: 1;
    padding: 15px;
    font-size: 14px;
    margin: ${margin};

    ::placeholder {
        font-size: 14px;
        text-transform: capitalize;
    }
`;

export const Select = styled.select`
    outline: none;
    border: 2px solid #cdcdcd;
    text-decoration: none;
    flex: 1;
    padding: 15px;
    font-size: 14px;
    margin: ${margin};

    ::placeholder {
        font-size: 14px;
        text-transform: capitalize;
    }
`;

export const Container = styled.div`
    display: block;
`;

export const Pair = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`;

export const TextArea = styled.textarea`
    width: 100%;
    padding: 20px;
    height: 160px;
    margin: ${margin};
    border: 2px solid #cdcdcd;
    resize: none;

    ::placeholder {
        font-size: 14px;
        text-transform: capitalize;
    }
`;

export const Submit = styled.button`
    outline: none;
    border: none;
    text-decoration: none;
    width: 100%;
    padding: 30px;
    color: ${colors.white};
    background-color: ${colors.dark};
    margin: ${margin};
`;
