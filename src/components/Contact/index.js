import React, { Component } from 'react'
import * as Page from '../../style/section'
import * as Form from './style'
import * as Type from '../../style/typeface'
import ContactForm from './Form';
import colors from '../../style/colors';

export default class Contact extends Component {
    render() {
        return (
            <Page.Container>
                <Page.Row>
                    <Page.Panel>
                        <Form.GraphicsContainer>
                            <Form.Overlay>
                                <Form.Content>
                                    <Page.Col ht="100%" >
                                        <Page.Panel>
                                            <Type.H1
                                                color={colors.white}
                                            >
                                                Hello,
                                            </Type.H1>
                                            <Type.Txt
                                                color={colors.white}
                                            >
                                                You’re welcome to visit
                                                us at our office, call
                                                us or drop us a note. We
                                                will love to hear the
                                                awesome idea you’re
                                                working
                                            </Type.Txt>
                                        </Page.Panel>
                                        <Page.Panel color={ colors.white }>
                                            <Type.H2>
                                                Our Office
                                            </Type.H2>
                                            <Type.Txt>
                                                15C, Chief Gbolahan
                                                Owolabi Street,
                                                <br /> Lekki Phase One,
                                                <br /> Lagos, Nigeria.
                                            </Type.Txt>
                                            <Page.Pad pad="20px 0 10px">
                                                <Type.Txt >
                                                    +234 (0) 8064826555
                                                </Type.Txt>
                                            </Page.Pad>
                                            <Page.Pad pad="10px 0">
                                                <Type.Txt>
                                                    hello@swipemax.com
                                                </Type.Txt>
                                            </Page.Pad>
                                            <Page.Pad>
                                                <Form.Media >
                                                    <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                                                        <i className="fab fa-twitter fa-2x"></i>
                                                    </a>
                                                    <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                                                        <i className="fab fa-linkedin fa-2x"></i>
                                                    </a>
                                                    <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                                                        <i className="fab fa-instagram fa-2x"></i>
                                                    </a>
                                                </Form.Media>
                                            </Page.Pad>
                                        </Page.Panel>
                                    </Page.Col>
                                </Form.Content>
                            </Form.Overlay>
                            <Form.Mage
                                src={require("../../assets/images/contact-images/office.jpg")}
                                alt="office"
                            />
                        </Form.GraphicsContainer>
                    </Page.Panel>
                    <Page.Panel>
                        <Page.Pad pad="60px">
                            <Form.Exit>
                                <i className="fas fa-times fa-3x" />
                            </Form.Exit>
                            <ContactForm />
                        </Page.Pad>
                    </Page.Panel>
                </Page.Row>
            </Page.Container>
        );
    }
}
