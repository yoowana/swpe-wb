import styled from 'styled-components'

export const Exit = styled.div`
    display: flex;
    margin-bottom: 60px;
    justify-content: flex-end;
    padding-right: 15px;
`

export const GraphicsContainer = styled.div`
    position: relative;
    height: 100%;
`;

export const Mage = styled.img`
    position: absolute;
    width: 100%;
    bottom: 0;
    z-index: 19;
`

export const Overlay = styled.div`
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 20;
    background: linear-gradient(180deg, rgba(130,42,184,1) 0%, rgba(121,42,173,1) 60%, rgba(131,41,184,0.8) 100%);

`

export const Col = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`

export const Content = styled.div`
    padding: 60px 60px 0 120px;
    height: 85%;
`;

export const Media = styled.div`
    margin-top: 80px;
    width: 50%;
    display: flex;
    justify-content: space-between;
`