import React from "react";
import * as Type from "../../style/typeface";
import colors from "../../style/colors";

const Values = ({ title, description }) => {
    return (
        <div style={{padding: "20px"}}>
            <Type.H3 size="22px" fw="300" color={colors.blueDark} > { title } </Type.H3>
            <Type.Txt color={ colors.blueDark }> { description } </Type.Txt>
        </div>
    );
};

export default Values;
