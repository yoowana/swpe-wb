export default [
    {
        title: "Everybody Contributes",
        description:
            "User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals."
    },
    {
        title: "Socialize and be communal",
        description:
            "User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals."
    },
    {
        title: "Always Deliver",
        description: "User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals."
    },
    {
        title: "Be Professional",
        description: "User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals."
    },
    {
        title: "Clients are part of the team",
        description: "User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals."
    }
];
