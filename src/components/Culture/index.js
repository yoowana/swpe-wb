import React, { Component } from "react";
import styled from 'styled-components'
import * as Page from "../../style/section";
import * as Type from "../../style/typeface";
import PoiCard from "./user-card";
import colors from "../../style/colors";
import values from "./data";
import Values from "./Values";
import data from "./user-card/data";

const H2Alt = styled(Type.H2)`
    line-height: 60px;
`

export default class Culture extends Component {
    render() {
        return (
            <Page.Section bgColor={colors.white}>
                <Page.Pad pad="40px 0 0">
                    <Type.H2
                        style={{ marginBottom: 30, textAlign: "center" }}
                        color={colors.blueDark}
                    >
                        {" "}
                        Our Culture{" "}
                    </Type.H2>
                    <Page.CardContainer min="250px">
                        {values.map((value, i) => {
                            return (
                                <Values
                                    title={value.title}
                                    description={value.description}
                                    key={i}
                                />
                            );
                        })}
                    </Page.CardContainer>
                    <Page.Row margin="74px 0">
                        <Page.Panel width="40vw" mwidth="100%">
                            <H2Alt> The humans of Swipe</H2Alt>
                        </Page.Panel>
                        <Page.Panel>
                            <Page.Pad pad="12px 0 0 100px" mpad="12px 0">
                                <Type.Txt lh="35px">
                                    Our team consists of creative and smart
                                    individuals with backgrounds in design,
                                    business and technology. We are focused on
                                    building functional and expressive designs
                                </Type.Txt>
                            </Page.Pad>
                        </Page.Panel>
                    </Page.Row>
                    <Page.ImageCardContainer min="175px" rowmin="175px">
                        {data.map((person, i) => {
                            return (
                                <PoiCard
                                    src={person.src}
                                    alt={person.alt}
                                    poi={person.poi}
                                    key={i}
                                />
                            );
                        })}
                    </Page.ImageCardContainer>
                    <div> video section</div>
                </Page.Pad>
            </Page.Section>
        );
    }
}
