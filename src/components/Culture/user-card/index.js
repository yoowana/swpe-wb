import React from "react";
import { Pad } from "../../../style/section"
import * as Card from "./style";
import * as Type from "../../../style/typeface";

class ImgCard extends React.Component{
    state = { 
        hovering: false
    }

    handleMouseEnter = e => {
        this.setState({
            hovering: true
        })
    }
    handleMouseLeave = e => {
        this.setState({
            hovering: false
        })
    }
    render(){
        const { src, alt, poi } = this.props
        const { hovering } = this.state
        return (
            <Card.Container onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}>
                <Card.Image src={src} alt={alt} />
                <Card.Info hovering={ hovering }>
                    <Pad pad="30% 0 0 0">
                        <Type.H3> {poi.name} </Type.H3>
                        <Type.Txt> {poi.title} </Type.Txt>
                    </Pad>
                </Card.Info>
            </Card.Container>
        );
    }
}

export default ImgCard;
