import styled from 'styled-components'
import colors from '../../../style/colors';

export const Container = styled.div`
    height: 100%;
    width: 100%;
    position: relative;
    overflow-y: hidden;
    `
    //background-image: ${ props => props.image };
    
    export const Image = styled.img`
    height: 100%;
    width: 100%;
    object-fit: cover;
    border-radius: 5px;
`

export const Info = styled.div`
    background-color: ${ colors.yellow };
    visibility: ${ props => props.hovering ? "visible" : "hidden"};
    position: absolute;
    top: 0;
    text-align: center;
    height: 100%;
    border-radius: 5px;
    width: 100%;
`