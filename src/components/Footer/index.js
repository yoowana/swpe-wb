import React from "react";
import * as Footer from "./style";
import * as Type from "../../style/typeface";

export default () => {
    return (
        <Footer.Container>
            <Footer.LeftSide>
                <Type.Txt>Swipemax 2019</Type.Txt>
            </Footer.LeftSide>
            <Footer.RightSide>
                <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-twitter fa-xl"></i>
                </a>
                <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-linkedin fa-xl"></i>
                </a>
                <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-instagram fa-xl"></i>
                </a>
            </Footer.RightSide>
        </Footer.Container>
    );
};
