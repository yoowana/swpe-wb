import styled from 'styled-components'
import screens from '../../style/screens';

export const Container = styled.div`
    padding: 30px 70px;
    display: flex;
    justify-content: space-between;

    @media screen and (max-width: ${ screens.phone}){
        padding: 30px 20px;
    }
`
export const RightSide = styled.div`
    display: flex;
    justify-content: space-between;
    min-width: 120px;
`
export const LeftSide = styled.div`
    display: block;
`