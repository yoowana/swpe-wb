import React from 'react'
import styled from 'styled-components'
import colors from '../../style/colors';

const Container = styled.div`
    position: relative;
    margin-left: -100px;
`
export const SVG = styled.svg`
    fill: ${ colors.amaranth };
    position: absolute;
    right: -250px;
`

const S = () => {
  return (
      <Container>
          <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 867.65 1024">
              <defs>
                  <clipPath id="clip-path">
                      <rect className="cls-1" width="867.65" height="1024" />
                  </clipPath>
              </defs>
              <g id="Layer_2" data-name="Layer 2">
                  <g id="Layer_1-2" data-name="Layer 1">
                      <g className="cls-2" id="Line">
                          <g className="cls-2">
                              <path className="cls-3" d="M861.73,809.74c15.28-60.34.1-119.78-35.57-171.65-26.46-38.48-64.54-73.41-112.9-105.76L365,299.27l319.58-28.54A135.63,135.63,0,1,0,660.42.55L269.28,35.49C211.4,40.66,161.31,53,119.71,73.88,63.42,102.2,21.48,147,6,207.47S5.79,327.38,41.58,379.25C68,417.58,106,452.39,154.32,484.7l351,234.87L149,753.34a135.63,135.63,0,0,0,25.59,270l424.67-40.25c57.91-5.48,108-18.12,149.58-39.38,56-28.67,97.64-73.71,112.92-134"
                              />
                          </g>
                      </g>
                  </g>
              </g>
          </SVG>
      </Container>
  )
}

export default S
