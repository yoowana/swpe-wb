import React, { Component } from "react";
import colors from "../../style/colors";
import * as Type from "../../style/typeface";
import * as Page from "../../style/section";

export default class Landing extends Component {
    render() {
        return (
            <div>
                <Page.Section
                    bgColor={colors.purpleDark}
                    color={colors.white}
                    className="landing-section"
                >
                    <Page.Content>
                        <Page.SubHero pad="40vh 0 0" width="560px">
                            <Type.H1 color={colors.seaBlue}>
                                DESIGN x BUILD
                            </Type.H1>
                            <Type.Txt>
                                From ideation to execution, we incorporate
                                functional and expressive design to build
                                delightable products for startups and
                                enterprise.
                            </Type.Txt>
                        </Page.SubHero>
                    </Page.Content>
                </Page.Section>
            </div>
        );
    }
}
