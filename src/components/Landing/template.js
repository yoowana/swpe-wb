import React, { Component } from "react";
import colors from "../../style/colors";
import * as Type from "../../style/typeface";
import * as Page from "../../style/section";
//import S from "./S";

/**
 * @param {Object} theme object: {bg: "string", color: "string"}
 * @param {String} title title of the page
 * @param {String} subtxt sub-text of the splash
 */

export default class AboutSplash extends Component {
    render() {
        const { theme, title, subtxt } = this.props;
        return (
            <div>
                <Page.Section
                    bgColor={theme.bg}
                    color={theme.color}
                    className="landing-section"
                >
                    <Page.Content>
                        <Page.Row>
                            <Page.SubHero pad="40vh 0 0" width="560px">
                                <Type.H1 color={theme}>{ title }</Type.H1>
                                <Type.Txt> { subtxt } </Type.Txt>
                            </Page.SubHero>
                            <Page.Panel style={{position: "relative"}}>
                                <Page.Mask src={require("../../assets/images/about-images/splash.jpg")} alt="splash" />
                            </Page.Panel>
                        </Page.Row>
                    </Page.Content>
                </Page.Section>
            </div>
        );
    }
}

AboutSplash.defaultProps = {
    theme: {
        bg: colors.yellow,
        color: colors.dark
    }
}
