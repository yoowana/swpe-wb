import React, { Component } from "react";
import { Link } from "react-router-dom"
import { TimelineLite } from "gsap/TweenMax";
import * as Type from "../../../style/typeface"
import * as Menu from "./style";
import Logo from "../../Logo";
//import W from "../../W";

export default class MenuComponent extends Component {
    state = {
        opened: false
    };

    menuRef = React.createRef();
    listRef = React.createRef();
    myTween = React.createRef();

    timeLine = React.createRef();
    list = React.createRef();

    menuClick = e => {
        const { opened } = this.state;
        this.timeLine = new TimelineLite();
        console.log(this.timeLine);

        this.setState(
            {
                opened: !opened
            },
            () => {
                // this.myTween = this.state.opened
                // ? TweenMax.to(this.menuRef, 1, {
                //     width: "100%"
                // })
                // : TweenMax.to(this.menuRef, 1, {
                //     width: 0
                // })
                this.state.opened
                    ? this.timeLine
                        .to(this.menuRef, 0.5, { width: "100%" })
                        .staggerTo(".linker", 0.5, { opacity: 1, right: 50 }, 0.1)
                    : this.timeLine
                        .staggerTo(".linker", 0.5, { opacity: 0 }, 0.1)
                        .to(this.menuRef, 0.5, { width: 0, delay: -0.25 });
            }
        );
    };

    render() {
        console.log(this.menuRef);
        console.log(this.linkA);

        const { opened } = this.state
        console.log( opened )

        return (
            <Menu.Container>
                <Menu.Button onClick={this.menuClick}>
                    <Logo theme={() => { return opened ? "#2f2f2f" : "#F5f5f5"}} />
                </Menu.Button>
                <Menu.Hidden ref={div => (this.menuRef = div)}>
                    <Menu.List ref={div => (this.list = div)}>
                        <Link to="/">
                            <Type.H1>
                                <Menu.NavLink className="linker" >Home</Menu.NavLink>
                            </Type.H1>
                        </Link>
                        <Link to="/projects">
                            <Type.H1>
                                <Menu.NavLink className="linker" >Projects</Menu.NavLink>
                            </Type.H1>
                        </Link>
                        <Link to="/career" >
                            <Type.H1>
                                <Menu.NavLink className="linker" >Career</Menu.NavLink>
                            </Type.H1>
                        </Link>
                        <Link to="/about">
                            <Type.H1>
                                <Menu.NavLink className="linker" >About</Menu.NavLink>
                            </Type.H1>
                        </Link>
                        <Link to="/contact">
                            <Type.H1>
                                <Menu.NavLink className="linker" >Contact</Menu.NavLink>
                            </Type.H1>
                        </Link>
                    </Menu.List>
                    <Menu.BG />
                </Menu.Hidden>
            </Menu.Container>
        );
    }
}