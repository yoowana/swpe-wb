import styled from "styled-components";
import colors from "../../../style/colors";

export const Container = styled.div`
    display: block;
    font-family: "Montserrat", sans-serif;
    font-size: 45px;
    position: fixed;
    width: 100%;
    z-index: 51;
`;
export const Hidden = styled.div`
           width: 0;
           position: absolute;
           height: 100vh;
           background-color: ${colors.white};
           z-index: 52;
           display: flex;
           justify-content: center;
           color: #fff;
           padding-top: 120px;
           background: #fff url(${require("../../../assets/svgs/W.svg")}) -317px
               80px/645px no-repeat;
       `;
export const Button = styled.div`
    cursor: pointer;
    width: 300px;
    position: absolute;
    z-index: 53;
    padding-left: 20px;
`;
export const List = styled.div`
    position: absolute;
    color: ${ colors.dark };
`;
export const NavLink = styled.div`
    opacity: 0;
`;
export const BG = styled.div`
    position: absolute;
`;
