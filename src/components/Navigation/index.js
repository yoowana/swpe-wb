import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as Nav from "./style";
import * as Page from "../../style/section"
import Logo from "../Logo";

export default class Navigation extends Component {
    state = {
        show: false
    }

    handleClick = e => {
        console.log(e.target);
    }
    render() {
        const { theme } = this.props
        return (
            <Nav.Header theme={theme}>
                <Page.Content>
                    <Nav.Container>
                        <Nav.Left color={ theme } >
                            <Link to="/">
                                <Logo theme={ theme } onClick={this.handleClick} />
                            </Link>
                        </Nav.Left>
                        <Nav.Right color={ theme } >
                            <Link to="/projects">Projects</Link>
                            <Link to="/about">About</Link>
                            <Link to="/career">Career</Link>
                            <Link to="/contact">Contact</Link>
                            <Nav.MenuBtn>
                                Btn
                            </Nav.MenuBtn>
                        </Nav.Right>
                    </Nav.Container>
                </Page.Content>
            </Nav.Header>
        )
    }
}
