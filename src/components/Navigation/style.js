import styled from 'styled-components'
import colors from '../../style/colors';
import screens from '../../style/screens';

export const Header = styled.header`
    display: block;
`
export const Container = styled.div`
    display: flex;
    flex-direction: row;
    position: fixed;
    width: 100%;
    padding-top: 20px;
    flex-flow: row nowrap;
    z-index: 50;
`
export const Left = styled.div`
    flex-basis: 100%;
    font-size: 32px;
    color: ${ props => props.color };
    padding-left: 120px;

    @media screen and ( max-width: ${ screens.phone }) {
        padding-left: 20px;
    }
`
Left.defaultProps = {
    color: colors.white
}

export const Right = styled.div`
    display: flex;
    flex-basis: 100%;
    padding-right: 120px;
    color: ${ props => props.color };
    justify-content: space-between;
    @media screen and ( max-width: ${ screens.phone }) {
        display: none;
    }
`
Right.defaultProps = {
    color: colors.white
}

export const MenuBtn = styled.div`
    display: none;
    @media screen and ( max-width: ${ screens.tabletPort }) {
        display: block
    }
`