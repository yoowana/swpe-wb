import React from "react";
import styled from "styled-components";

const Container = styled.div`
    color: ${ props => props.color};
`;

const Number = ({ current, total, color }) => {
    return (
        <Container color={color} >
            {current} / {total}
        </Container>
    );
};

export default Number;
