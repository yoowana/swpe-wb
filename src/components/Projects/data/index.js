export default [
    {
        title: "Test Title A",
        description: "This is a test Description for test title A.",
        categories: [ "Branding", "Javascript" ],
        colors: {
            main: "#FFF",
            bgColor: "#3D2ABF",
            text: "#fff"
        }
    },
    {
        title: "Test Title B",
        description: "This is a test Description for test title B.",
        categories: ["product design", "prototyping"],
        colors: {
            main: "#EA2D6B",
            bgColor: "#FFE8FC",
            text: "#fff" 
        }
    }
]