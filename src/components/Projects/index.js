import React, { Component } from "react";
import numpad from "lodash.padstart"; //used to format number
import * as Page from "../../style/section";
import Project from "./project";
import projectList from "./data";
import Number from "../Number";
import Track from "../Track";

export default class Projects extends Component {
    state = {
        loaded: false,
        position: 0
    };

    handleRight = e => {
        console.log("Right Works")
        this.setState({
            position: this.state.position + 1
        })
    }

    handleLeft = e => {
        console.log("Left Works")
        this.setState({
            position: this.state.position - 1
        })
    }


    render() {
        const { position } = this.state;

        const prjct = projectList[position];
        const length = projectList.length;
        const mainColor = prjct.colors.main;

        return (
            <Page.Section bgColor={prjct.colors.bgColor}>
                <Page.Content>
                    <Page.Row>
                        <Page.ProjectCol ht="580px" pad="20px 0" margin="40px 0 0 0" >
                            <Number
                                current={numpad((position + 1).toString(), 2, "0")}
                                total={numpad(length.toString(), 2, "0")}
                                color={mainColor}
                            />
                            <Track color={mainColor} onLeftArrow={this.handleLeft} onRightArrow={this.handleRight} />
                        </Page.ProjectCol>
                        <Project
                            title={prjct.title}
                            desc={prjct.description}
                            categories={prjct.categories}
                            colors={prjct.colors}
                        />
                    </Page.Row>
                </Page.Content>
            </Page.Section>
        );
    }
}
