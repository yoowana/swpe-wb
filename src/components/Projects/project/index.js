import React from "react";
import * as Case from "./style";
import * as Type from "../../../style/typeface";

/**
 *
 * @param {title} title string
 * @param {String} desc
 * @param {String} categories
 */

const Project = ({ title, desc, categories, colors }) => {
    return (
        <Case.Container bgColor={colors.bgColor}>
            <Case.Bg>
                <Case.BgText>
                    {title}
                </Case.BgText>
                <Case.Row>
                    <Case.Panel>
                        <Case.Pad pad="140px 0 0 60px" mpad="0" >
                            <Case.Title>
                                <Type.H2 color={colors.main}>{title}</Type.H2>
                            </Case.Title>
                            <Case.Description>
                                <Type.Txt color={colors.main}>{desc}</Type.Txt>
                            </Case.Description>
                            <Case.Categories color={colors.main}>
                                {categories.map((category, i) => {
                                    const last = (categories.length - 1) === i
                                    console.log(last);
                                    return <Case.Category last={last} color={colors.main} key={i} > { category } </Case.Category>
                                })}
                            </Case.Categories>
                        </Case.Pad>
                    </Case.Panel>
                    <Case.Panel>
                        <Case.ImgContainer>
                            <Case.Img src={require("../../../assets/images/project-images/testa.jpg")} alt="placeholder" />
                        </Case.ImgContainer>
                    </Case.Panel>
                </Case.Row>
            </Case.Bg>
        </Case.Container>
    );
};

export default Project;
