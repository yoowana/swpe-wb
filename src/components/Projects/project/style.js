import styled from 'styled-components'
import screens from '../../../style/screens';


const padAdjust = `20px`

export const Container = styled.div`
    display: block;
    width: 100%;
`
export const Row = styled.div`
    display: flex;
    flex-direction: row;
    position: absolute;
    top: 20px;
    justify-content: space-between;
    width: 100%;
    flex-wrap: wrap;

    @media screen and ( max-width: ${ screens.tabletPort }){
        flex-direction: column;
        flex-flow: column-reverse;
    }
`
export const Title = styled.div`
    display: block;
    margin-bottom: 30px;
    padding-left: ${padAdjust};
    @media screen and ( max-width: ${ screens.tabletPort }){
        padding: 40px 0 0 0;

    }
`
export const Description = styled.div`
    display: block;
    height: 200px;
    margin-bottom: 20px;
    padding-left: ${padAdjust};

    @media screen and ( max-width: ${ screens.tabletPort }){
        height: auto;
        padding: 0;
    }
`
export const Panel = styled.div`
    flex-basis: 50%;
`
export const Pad = styled.div`
    padding: ${ props => props.pad }; 
    @media screen and ( max-width: ${ screens.tabletPort }){
        padding: ${ props => props.mpad };         
    }
`
export const Bg = styled.div`
    position: relative;
    user-select: none;
`
//need to fix padding top
export const BgText = styled.div`
    text-align: center;
    font-size: 200px;
    color: rgba(0,0,0,0.05);
    padding-top: 20%; 
`
export const Categories = styled.div`
    display: block;
    color: ${ props => props.color };
    @media screen and ( max-width: ${ screens.tabletPort }){
        margin-left: -20px;
    }
`
export const Category = styled.span`
    border-right: ${ props => props.last ? null : `2px solid ${ props.color }`};
    padding: 0 20px;
    font-size: 24px;
`
export const Case = styled.div`
    display: block;
`
export const Img = styled.img`
    object-fit: cover;
    height: 520px;
    width: 100%;
    position: absolute;
    right: -120px;

    @media screen and ( max-width: ${ screens.tabletPort }){
        position: static;
    }
    
`
export const ImgContainer = styled.div`
    margin-top: 100px;
    width: 100%;
    align-items: right;
    position: relative;
    @media screen and ( max-width: ${ screens.tabletPort }){
        margin-top: 0
    }
`
