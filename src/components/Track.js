import React from "react";
import styled from 'styled-components'


const Container = styled.div`
    display: block;
`
const Left = styled.div`
    cursor: pointer;
`
const Right = styled.div`
    cursor: pointer;
`
const Carets = styled.div`
    display: flex;
    justify-content: space-between;
`


const Track = ({ color, onLeftArrow, onRightArrow }) => {
    return (
        <Container style={{ color: color }}>
            tracker
            <Carets>
                <Left onClick={onLeftArrow}><i class="fas fa-caret-left"></i></Left>
                <Right onClick={onRightArrow}><i className="fas fa-caret-right"></i></Right>
            </Carets>
        </Container>
    );
};

export default Track;
