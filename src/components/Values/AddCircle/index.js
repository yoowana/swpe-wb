import React from "react";
import * as Circle from './style'


const AddCircle = () => {
    return (
        <Circle.Container>
            <Circle.Outer>
                <Circle.Main>
                    +
                </Circle.Main>
            </Circle.Outer>
        </Circle.Container>
    );
};

export default AddCircle;
