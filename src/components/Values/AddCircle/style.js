import styled from 'styled-components'
import colors from '../../../style/colors'

export const Container = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
`
export const Outer = styled.div`
    display: flex;
    justify-content: center;
    width: 288px;
    height: 288px;
    border-radius: 50%;
    border: 2px dotted ${ colors.dark };
    padding: 16px;
    position: absolute;
    top: -162px;
`
export const Main = styled.div`
    height: 140px;
    margin-top: 47px;
    padding-top: 30px;
    font-size: 80px;
    font-weight: 500;
    width: 140px;
    border-radius: 50%;
    border: 7px solid ${ colors.purpleDark };
    text-align: center;
    padding: 20px ;
    line-height: 140px;
    box-shadow: 0px 4px 24px rgba(116, 116, 116, 0.25);
    background-color: ${ colors.gray };
`