import React, { Component } from "react";
import * as Page from "../../style/section";
import * as Type from "../../style/typeface";
import Circle from "./AddCircle";
import colors from "../../style/colors";

export default class Values extends Component {
    render() {
        return (
            <Page.Section bgColor={colors.gray}>
                <Page.Row>
                    <Page.Panel>
                        <Page.Pad pad="220px 40px 40px">
                            <Type.H2> Creative </Type.H2>
                            <Type.Txt>
                                We are a team of creative minds constantly inspiring
                                and motivating ourselves throughout project cycles
                                in order to bring out our best
                            </Type.Txt>
                        </Page.Pad>
                    </Page.Panel>
                    <Page.Panel >  
                        <Page.IMG src={require("../../assets/images/about-images/creative.jpg")} alt="creativity" />
                    </Page.Panel>
                </Page.Row>
                <Circle />
                <Page.Row>
                    <Page.Panel >
                        <Page.IMG src={require("../../assets/images/about-images/open.jpg")} alt="openess" />
                    </Page.Panel>
                    <Page.Panel>
                        <Page.Pad pad="220px 40px 40px">
                            <Type.H2> Open </Type.H2>
                            <Type.Txt>
                                Collaboration is proven factor of a success product
                                development. Here at Swipemax we are open to inputs
                                or contributions from everybody including clients
                            </Type.Txt>
                        </Page.Pad>
                    </Page.Panel>
                </Page.Row>
            </Page.Section>
        );
    }
}
