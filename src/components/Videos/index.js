import React, { Component } from 'react'
import * as Video from './style'

export default class VideoSection extends Component {
    render() {
        return (
            <Video.Container>
                <iframe src="https://player.vimeo.com/video/276004055" title="xyjsh" width="100%" height="600px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </Video.Container>
        )
    }
}
