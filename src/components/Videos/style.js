import styled from 'styled-components'
import colors from '../../style/colors';

export const Container = styled.div`
    display: block;
    display: flex;
    justify-content: center;
    padding: 40px 0;
    border-bottom: 0.5px solid ${ colors.gray };
`
