import React from 'react'
import styled from 'styled-components'
import colors from '../style/colors';
import { TimelineLite } from 'gsap/TweenMax';


const SVG = styled.svg`
    fill: ${colors.gray};
    display: none;
    position: absolute;
    z-index: -2;
    height: 500px;
    left: -386px;
    top: -72px;
`;


class W extends React.Component{

    show = React.createRef()
    svgRef = React.createRef()

    componentDidMount(){
        this.show = new TimelineLite()
        this.show.to( this.svgRef, 1, {
            display: "block"
        })
    }

    render(){
        return (
            <SVG ref={ div => this.svgRef = div } xmlns="http://www.w3.org/2000/svg" viewBox="0 0 187.51 151.03">
                <defs />
                <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                        <g id="Page-1">
                            <g id="Artboard">
                                <path id="Line-Copy-6" className="cls-1" d="M132.12,70.54,112,21.86C105.3,5.69,82.55,5.3,75.3,21.22l-22,48.41-13.9-54.1a20,20,0,1,0-38.74,10l28,109c4.69,18.28,29.76,20.48,37.58,3.31L92.63,79.71l24.39,59c7.23,17.49,32.46,16.08,37.7-2.11l32-111a20,20,0,1,0-38.44-11.08Z"
                                />
                            </g>
                        </g>
                    </g>
                </g>
            </SVG>
        )
    }
}

export default W
