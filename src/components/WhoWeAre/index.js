import React, { Component } from "react";
import styled from 'styled-components'
import numpad from "lodash.padstart"
import * as Page from "../../style/section";
import * as Type from "../../style/typeface"
import colors from "../../style/colors";
import Number from "../Number";
import Track from "../Track";
import screens from "../../style/screens";


const Mobile = styled.div`
    @media screen and (max-width: ${screens.phone}) {
       display: none; 
    }
`

export default class About extends Component {
    state = {
        position: 0
    }

    render() {
        const { position } = this.state
        return (
            <Page.Section bgColor={colors.main} >
                <Page.Content>
                    <Page.Row>
                        <Mobile>
                            <Page.Col ht="580px" pad="20px 0" margin="40px 0 0 0" >
                                <Number
                                    current={numpad((position + 1).toString(), 2, "0")}
                                    total={2}
                                    color={colors.white}
                                />
                                <Track color={colors.white} />
                            </Page.Col>
                        </Mobile>
                        <Page.Panel >
                            <Page.Pad pad="60% 0 0 40px" mpad="40px 0 0 0">
                                <Type.H2 color={colors.white} > What we do</Type.H2>
                            </Page.Pad>
                        </Page.Panel>
                        <Page.Panel>
                            <Page.Pad pad="120px 0 0 0" mpad="20px 0 60px 0" style={{color: colors.white}}>
                                <Page.Box>
                                    <Type.H3> Strategy </Type.H3>
                                    <Type.Txt> User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals. </Type.Txt>
                                </Page.Box>
                                <Page.Box>
                                    <Type.H3> Design </Type.H3>
                                    <Type.Txt> User-centered design approach to designing products that solves users problems which in turn creates a delightful experience and meet organisation’s goals. </Type.Txt>
                                </Page.Box>
                                <Page.Box>
                                    <Type.H3> Develop </Type.H3>
                                    <Type.Txt>Follow industry standards to product building while making use of cutting-technology, finest of minds</Type.Txt>
                                </Page.Box>
                                Get in touch->
                            </Page.Pad>
                        </Page.Panel>
                    </Page.Row>
                </Page.Content>
            </Page.Section>
        );
    }
}
