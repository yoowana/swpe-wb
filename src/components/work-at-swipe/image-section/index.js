import React, { Component } from 'react'
import images from './images'
import * as Slide from './style'

export default class MageSection extends Component {
    render() {
        return (
            <Slide.Container>
                {
                    images.map( (image, i) => {
                        return <Slide.Mage src={image} alt="swipe photo" key={ i } />
                    })
                }
            </Slide.Container>
        )
    }
}
