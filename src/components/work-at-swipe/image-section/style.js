import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    overflow-x: scroll;
    ::-webkit-scrollbar {
        width: 0px;  /* remove scrollbar space */
        background: transparent;  /* optional: just make scrollbar invisible */
    }
    /* optional: show position indicator in red */
    ::-webkit-scrollbar-thumb {
        background: transparent;
    }
`
export const Mage = styled.img`
    margin: 10px;
    height: 200px;
    border-radius: 5px;
    
`