import React, { Component } from 'react'
import * as Type from '../../style/typeface'
import * as Career from './style'

export default class WorkingAtSwipe extends Component {
  render() {
    return (
        <Career.Container>
            <Career.Main>
                <div>
                    <Type.H2>Working At Swipe</Type.H2>
                    <Type.Txt>
                        We want to hire people with a passion for digital
                        products. At Swipe, you will work with a unified
                        product team. Here everybody in our team gets to
                        make a real difference. So we want you to feel
                        empowered, motivated and challenged in order to
                        create your best work.
                    </Type.Txt>
                </div>
            </Career.Main>
        </Career.Container>
    );
  }
}
