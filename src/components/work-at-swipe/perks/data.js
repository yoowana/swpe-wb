export default [
    {
        title: "Well-Deserved Pay",
        description:
            "Living in Lagos can be tough. We value hard work and make sure the team is covered accordingly."
    },
    {
        title: "Vacation Mode",
        description:
            "We offer PTO, including paid sick days, paid holidays, and paid vacation days—so treat yourself."
    },
    {
        title: "Great Atmostphere",
        description:
            "Work alongside likeminded individuals in an environment that fosters creativity and embraces growth."
    },
    {
        title: "Health is Wealth",
        description:
            "We offer flexible health benefits including dental, vision, and more."
    }
];