import React, { Component } from 'react'
import * as Type from '../../../style/typeface'
import * as Page from '../../../style/section'
import { Container } from './style'
import benefits from './data'
import Card from '../../Culture/Values'


export default class Perks extends Component {
    render() {
        return (
            <Container>
                <Type.H2> Perks &amp; Benefits </Type.H2>
                <Page.CardContainer min="250px" style={{ textAlign: "left"}} >
                    {benefits.map((benefit, i) => {
                        return (
                            <Card
                                title={benefit.title}
                                description={benefit.description}
                                key={i}
                            />
                        );
                    })}
                </Page.CardContainer>
            </Container>
        );
    }
}
