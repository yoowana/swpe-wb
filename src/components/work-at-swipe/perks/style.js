import styled from 'styled-components'
import colors from '../../../style/colors';

export const Container = styled.div`
    text-align: center;
    background-color: ${ colors.gray };
    padding: 40px 120px;
    height: 100%;
`