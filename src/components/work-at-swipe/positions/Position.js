import React from 'react'
import styled from 'styled-components'
import * as Type from '../../../style/typeface'
import colors from '../../../style/colors';


const Container = styled.div`
    background-color: ${ colors.gray };
    padding: 20px;
    margin-bottom: 10px;
`
const Body = styled.div`
    display: ${ props => props.show ? "block" : "none" };
    border-top: 0.5px solid #c4c4c4;
`
const Head = styled.div`
    display: flex;
    justify-content: space-between;
    cursor: pointer;
`
const Caret = styled.div`
    padding-top: 15px;
    :hover{
        color: ${ colors.amaranth };
    }
`

class Position extends React.Component{
    state = {
        show: false
    }

    handleClick = e => {
        const { show } = this.state
        const shower = ( currentIndex ) => {
            console.log('shower is working');
            if (this.props.index === currentIndex && show !== true) {
                this.setState({
                    show: !this.state.show
                });
            }
            else {
                this.setState({
                    show: false
                });
            }
        }
        this.props.onClick(shower);
    }

    componentWillReceiveProps(prev, next){
        if ((this.state.show === true) && (this.props.index !== next.currentIndex)) {
            this.setState({
                show: !this.state.show
            })
        }
    }


    render() {
        const { show } = this.state
        const { position } = this.props

        const Arrow = show
            ? () => <i className="fas fa-caret-up" />
            : () => <i className="fas fa-caret-down" />

        return (
            <Container>
                <Head onClick={this.handleClick}>
                    <Type.H3> {position.title} </Type.H3>
                    <Caret>
                        <Arrow />
                    </Caret>
                </Head>
                <Body show={show}>
                    <Type.Txt>{position.body}</Type.Txt>
                </Body>
            </Container>
        );
    }
} 


export default Position
