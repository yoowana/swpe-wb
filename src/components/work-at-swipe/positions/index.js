import React, { Component } from 'react'
import styled from 'styled-components'
import Position from './Position'
import positionList from './data'
import * as Type from '../../../style/typeface'


const Container = styled.div`
    padding: 40px 120px;
    text-align: center;
`

const Txt = styled.div`
    padding-bottom: 20px;
`

export default class Positions extends Component {
    state = {
        currentIndex: null
    }

    handleDropDown = ( e, index, func ) => {
        this.setState({ 
            currentIndex: index
        },
        () => func( this.state.currentIndex ))
    }

    render() {
        const { currentIndex } = this.state
        return (
            <Container>
                <Type.H2>Open Positions</Type.H2>
                <Txt>
                    <Type.Txt>
                        We are always on the lookout for skilled and
                        passionate people. If you think that might be you,
                        we would love to hear from you.
                    </Type.Txt>
                </Txt>
                <div>
                    {
                        positionList.map((position, i) => {
                            return <Position index={i} currentIndex={currentIndex} position={position} key={i} onClick={(func) => this.handleDropDown(null, i, func)} />
                        })
                    }
                </div>
            </Container>
        )
    }
}
