import styled from 'styled-components'

export const Container = styled.div`
    padding: 40px 120px;
    overflow: hidden;
`
export const Main = styled.div`
    display: block;
    text-align: center;
`
export const Txt = styled.div`
    padding-bottom: 20px;
`