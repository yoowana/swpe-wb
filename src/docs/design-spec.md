# Design Spec

Documentation for all design related materials pertaining to the swipe website.

# Docs

---

iOS and Android apps to offer our content to users wherever they are to complement the desktop experience.

# Why?

---

1. One of the biggest requests from our user base with close to 50 tickets per week.
2. Users are consuming more and more content on their phones - for some, it's the dominant form of consumption.
3. Complements the broader user experience on our platform more holistically by allowing users to access content wherever they are.

# How?

---

Framer Prototype v1: initial idea on how a piece of content should look and feel on iOS

Tap image to expand 

[https://framer.cloud/TFPWZ](https://framer.cloud/TFPWZ)

# Design Goals

---

- [ ]  Website is fully responsive.

# Design Notes

---

1. NPS on mobile experience
2. Retention of users that are using us cross-platform (desktop + mobile)
3. # of articles being accessed weekly
    - Current benchmark: ~3 articles read per week per user