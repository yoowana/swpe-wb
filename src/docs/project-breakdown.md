# Project break down and tasks

# Pages

- Home
- Projects
- Career
- About Us
- Contact Us

## Home

---

### Sections

- Splash/Hero
- Project minisections
- Who we are
- Past Clients
- Footer

### Notable Components

- Spash Background
- Projects section Interaction
- Phone Component

## Projects

---

### Sections

- Splash/Hero
- Project minisections
- Who we are
- Past Clients
- Footer

### Notable Components

- Spash Background Clipping Mask Image

## Career

---

### Sections

- Splash/Hero
- Working at Swipe
- Perks & Benefits
- Video Section
- Footer

### Notable Components

[Open Positions](./Open-Positions-9fd5c4df-4b0b-4ebb-a6d8-eb702afae3b5.md)

## About Us

---

### Sections

- Splash/Hero
- Values (Creative + Open)
- Our Culture
- What we do
- Past Clients
- Footer

### Notable Components

- Spash Background
- Values (Creative + Open)

Notes: need to remove past clients from all pages beside home and about.

## Contact us

---

### Sections

- Greeting Section
- Form Section

### Notable Components

- Form

[Copy of Open Positions](./Copy-of-Open-Positions-7deeb35a-b977-4a5c-a7c2-05e21c758d52.md)