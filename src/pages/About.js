import React, { Component } from "react";
import { Helmet } from "react-helmet"
import Splash from "../components/Landing/template";
import * as Page from "../style/section";
import Navigation from "../components/Navigation";
import colors from "../style/colors";
import Values from "../components/Values";
import Culture from "../components/Culture";
import Clients from "../components/Clients";
import Footer from "../components/Footer";
import WhoWeAre from "../components/WhoWeAre"

const txt =
    "At Swipe we create delight. We turn your ideas into great products by combining functional designs with maximum usability.";

export default class About extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Swipemax - About</title>
                    <meta name="theme-color" content={colors.yellow} />
                    <meta name="msapplication-navbutton-color" content={colors.yellow} />
                    <meta name="apple-mobile-web-app-status-bar-style" content={colors.yellow} />
                </Helmet>
                <Page.Container>
                    <Navigation theme={colors.dark} />
                    <Splash
                        theme={{ color: colors.dark, bg: colors.yellow }}
                        title="ABOUT US"
                        subtxt={txt}
                    />
                    <Values />
                    <Culture />
                    <WhoWeAre />
                    <Clients />
                    <Footer />
                </Page.Container>
            </div>
            
        );
    }
}
