import React, { Component } from "react";
import Splash from "../components/Landing/template";
import * as Page from "../style/section";
import Navigation from "../components/Navigation";
import colors from "../style/colors";
import Footer from "../components/Footer";
import WorkingAtSwipe from "../components/work-at-swipe";
import Perks from "../components/work-at-swipe/perks";
import Positions from "../components/work-at-swipe/positions";
import MageSection from "../components/work-at-swipe/image-section";
import VideoSection from "../components/Videos";

export default class Career extends Component {
    render() {
        return (
            <Page.Container>
                <Navigation theme={colors.white} />
                <Splash
                    title="CAREERS"
                    subtxt="At Swipe we create delight. We turn your ideas into great products by combining functional designs with maximum usability"
                    theme={{ bg: colors.main, color: colors.white }}
                />
                <WorkingAtSwipe />
                <MageSection />
                <Positions />
                <Perks />
                <VideoSection />
                <Footer />
            </Page.Container>
        );
    }
}
