import React, { Component } from "react";
import * as Page from "../style/section";
import Navigation from "../components/Navigation";
import colors from "../style/colors";
import Contact from "../components/Contact";


export default class ContactPage extends Component {
  render() {
    return (
      <Page.Container>
        <Navigation theme={colors.dark} />
        <Contact />
      </Page.Container>
    );
  }
}