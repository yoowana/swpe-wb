import React, { Component } from "react";
import { Helmet } from "react-helmet";
//import Navigation from "../components/Navigation"
import Nav2 from "../components/Navigation/animated"
import * as Page from "../style/section";
import Projects from "../components/Projects";
import Landing from "../components/Landing";
import WhoWeAre from "../components/WhoWeAre"
import Footer from "../components/Footer";
import Clients from "../components/Clients"
import colors from "../style/colors";

export default class Home extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Swipemax - Home</title>
                    <meta name="theme-color" content={colors.purpleDark} />
                    <meta name="msapplication-navbutton-color" content={colors.yellow} />
                    <meta name="apple-mobile-web-app-status-bar-style" content={colors.yellow} />
                </Helmet>
                <Nav2 />
                <Page.Container className="home-page page-container">
                    <Landing />
                    <Projects />   
                    <WhoWeAre />
                    <Clients />
                    <Footer />
                </Page.Container>
            </div>
        );
    }
}
