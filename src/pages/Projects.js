import React, { Component } from "react";
import Navigation from "../components/Navigation"
import * as Page from "../style/section";
import colors from "../style/colors";
import Projects from "../components/Projects";
import Splash from "../components/Landing/template";
import WhoWeAre from "../components/WhoWeAre"
import Footer from "../components/Footer";
import Clients from "../components/Clients"

export default class Home extends Component {
  render() {
    return (
      <Page.Container className="home-page page-container">
        <Navigation />
        <Splash 
          title="PROJECTS"
          subtxt="At Swipe we create delight. We turn your ideas into great products by combining functional designs with maximum usability."
          theme={{ bg: colors.amaranth, color: colors.white}} />
        <Projects />
        <WhoWeAre />
        <Clients />
        <Footer />
      </Page.Container>
    );
  }
}
