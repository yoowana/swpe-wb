export default {
    main: "#AB004F",
    white: "#fff",
    dark: "#2F2F2F",
    gray: "#F5F5F5",
    blueDark: "#283754",
    lightPink: "#FFE8FC",
    purpleDark: "#07003B",
    purple: "#3D2ABF",
    yellow: "#F9D510",
    amaranth: "#EF2B4E",
    seaBlue: "#00DEED"
}