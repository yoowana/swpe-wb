/**
 * @description screen sizes for desktop, tabletLand, tabletPort, phone
 * @field desktop
 * @var tabletLand lanscape screen size for Tablet mode
 * @var tabletPort portrait screen size for Portrait mode
 * @var phone phone screen size
 */
export default {
    "desktop": "1200px",
    "tabletLand": "900px",
    "tabletPort": "600px",
    "phone": "599px"
}

