import styled from "styled-components";
import screens from "./screens"


export const Container = styled.div`
    display: block;
`

export const Col = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: ${ props => props.ht };
    padding: ${ props => props.pad };
    margin: ${ props => props.margin };
`
Col.defaultProps = {
    ht: "auto"
}

export const ProjectCol = styled(Col)`
    @media screen and (max-width: ${ screens.phone }){
        height: auto;
        flex-direction: row;
    }
`

export const Section = styled.div`
    min-height: 720px;
    padding: 40px 120px;
    color: ${ props => props.color };
    background-color: ${ props => props.bgColor };
    overflow: hidden;
    @media screen and (max-width: ${screens.phone}) {
        padding: 0 20px;
    }
`
export const Content = styled.div`
    max-width: 1366px;
    margin: 0 auto;
`

export const Row = styled.div`
    display: flex;
    flex-direction: row;
    margin: ${ props => props.margin };

    @media screen and (max-width: ${screens.phone}) {
        flex-direction: column;
    }   
`

export const Pad = styled.div`
    padding: ${ props => props.pad}; 
    color: ${ props => props.color};
    @media screen and (max-width: ${screens.phone}) {
        padding: ${ props => props.mpad};         
    }  
`
export const Box = styled.div`
    display: block;
    margin-bottom: 40px;
`

export const Panel = styled.div`
    width: ${ props => props.width };
    padding: ${ props => props.pad };
    margin: ${ props => props.margin };
    color: ${ props => props.color };

    @media screen and (max-width: ${screens.phone}) {
        max-width: ${ props => props.mwidth };
    } 
`
Panel.defaultProps = {
    width: "100%"
}

export const SubHero = styled(Panel)`
    width: auto;
    max-width: ${ props => props.width};
`

export const IMG = styled.img`
    width: 100%;
    height: 600px;
    object-fit: cover;
`
export const CardContainer = styled.div`
    display: grid;
    grid-template-columns: repeat( auto-fit, minmax( ${ props => props.min }, 1fr));
    grid-column-gap: 20px;
    grid-row-gap: 20px;
`
export const ImageCardContainer = styled.div`
    display: grid;
    grid-template-columns: repeat( auto-fit, minmax(${ props => props.min }, 1fr));
    /* grid-template-rows: repeat( auto-fit, ${ props => props.rowmin }); */
    grid-column-gap: 50px;
    grid-row-gap: 30px;
`

export const Mask = styled.img`
    width: 150%;
    mask: url(${require("../assets/svgs/Stest.svg")}) no-repeat;
    position: absolute;
    right: -550px;
    top: -50px
`