import styled from "styled-components";
import screens from './screens'

const headerFont = "'Montserrat', sans-serif";
const mainFont = `"HK Grotesk", "Roboto", "Fira Sans", sans - serif`;
//Font Face
export const H1 = styled.div`
    font-family: ${headerFont};
    font-weight: 800;
    font-size: 62px;
    margin-bottom: 20px;
    color: ${props => props.color};
    @media screen and ( max-width: ${screens.phone}) {
        font-size: 42px;
        line-height: 46px;
    }
`;
export const H2 = styled.div`
    font-family: ${mainFont};
    font-weight: bold;
    line-height: 79px;
    font-size: 44px;
    color: ${props => props.color};

    @media screen and ( max-width: ${screens.tabletLand}) {
        font-size: 36px;
        line-height: 60px;
    }
    @media screen and ( max-width: ${screens.tabletPort}) {
        font-size: 36px;
        line-height: 60px;
    }
    @media screen and ( max-width: ${screens.phone}) {
        font-size: 32px;
        line-height: 36px;
    }
`;
export const H3 = styled.div`
    font-family: ${mainFont};
    font-weight: 500;
    line-height: ${props => props.lh};    
    font-size: ${props => props.size};
    color: ${props => props.color};
`;
H3.defaultProps = {
    lh: "43px",
    size: "24px"
};
export const SubHead = styled.div`
    line-height: 31px;
    font-size: 23px;
`
export const Txt = styled.div`
    font-family: ${mainFont};
    line-height: ${props => props.lh};
    font-size: 18px;
    color: ${props => props.color};
    font-weight: ${ props => props.fw }
`;
Txt.defaultProps = {
    lh: "26px",
    fw: "normal"
};
