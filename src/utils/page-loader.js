import loadable from "loadable-components";
import Loading from "../components/Loader";

export const Home = loadable(() => import("../pages/Home"), {
    LoadingComponent: Loading
});