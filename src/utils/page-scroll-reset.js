import { Component } from 'react'
import { withRouter } from 'react-router-dom'

/**
 * Resets the page scroll position , so that when a previously viewed page is visited,
 * the scroll position is reset. Created to solve the problem of a page remaining at a 
 * scroll position of the last visit.
 */
class ScrollToTop extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            window.scrollTo(0, 0)
        }
    }

    render() {
        return this.props.children
    }
}

export default withRouter(ScrollToTop)
